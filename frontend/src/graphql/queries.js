import {gql} from 'apollo-boost';
const getEmployeesQuery =gql `
	{
		employees{
			id 
			firstName
			lastName
			position
			teamId
			team{
				name
			}
		}
	}

`;

const getEmployeeQuery = gql `
	query ($id: ID) {
		employee(id: $ID){
			id
			firstName
			lastName
			position
			teamId
			team{
				name
			}
		}
	}
`;

export { getEmployeeQuery, getEmployeesQuery};