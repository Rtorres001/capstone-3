import {gql} from 'apollo-boost';

const loginMutation = gql`
	mutation(
		$email: String!
		$password: String!
	){
		loginUser(
			email: $email
			password: $password
		){
			firstName
			lastName
			isAdmin
			token
		}
	}
`;

const registerUserMutation = gql`
	mutation(
		$firstName: String!
		$lastName: String!
		$email: String!
		$password: String!
	){
		registerUser(
			firstName: $firstName
			lastName: $lastName
			email: $email
			password: $password
		) 
	}
`;
export { loginMutation, registerUserMutation};