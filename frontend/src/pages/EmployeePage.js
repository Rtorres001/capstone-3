import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns } from 'react-bulma-components';
import { graphql } from 'react-apollo'; //to pass query as props

import EmployeeAdd from '../components/forms/EmployeeAdd';


import { getEmployeesQuery } from '../graphql/queries';

const EmployeePage = props => {
	// console.log(props.data.assets);

	const data = props.data;

	return (
		<Section size='medium'>
	      <Heading>Employee</Heading>
	      <Columns>
	        <Columns.Column>
	          <EmployeeAdd />
	        </Columns.Column>
	        <Columns.Column>
	         
	        </Columns.Column>
	      </Columns>
	    </Section>
	)
}

export default graphql(getEmployeesQuery)(EmployeePage);