import React, { useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import verifyToken from './jwt-verify';


//pages
import AppNavbar from './partials/AppNavbar';
import EmployeePage from './pages/EmployeePage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import DashboardPage from './pages/DashboardPage';
//auth route
const AuthRoute = ({token, ...props})=> {
	return token ? <Route {...props} /> : <Redirect to="/login" />

}


const AdminRoute=({token, isAdmin, ...props}) => {
	if (token && isAdmin){
		return <AuthRoute {...props} token ={token} />
	} else if (token && !isAdmin){
		return <Redirect to= "/dashboard"/>
	} else {
		return <Redirect to="/login" />
	}
}

const App =() =>{
	const [token, setToken] = useState(localStorage.getItem('token'));
	const decoded = verifyToken(token);
	const [isAdmin, setIsAdmin] = useState(decoded ? decoded.isAdmin : null);
	const [firstName, setFirstName] = useState(decoded ? decoded.firstName : null);

	const updateSession = () => {
		setToken(token);
		setIsAdmin(decoded.isAdmin);
		setFirstName(decoded.firstName);
	}
	const Login =(props) => <LoginPage {...props} token={token} updateSession={updateSession} />

	const Logout =(props) =>{
		localStorage.clear();
		updateSession();
		window.location='/login';

	}

	const currentUser = () => {
		return { firstName, isAdmin, token }
	}

	const Dashboard = (props) => <DashboardPage {...props} currentUser={currentUser} updateSession={updateSession}/>

	return (
		<BrowserRouter>
			<AppNavbar firstName={firstName} isAdmin={isAdmin} token={token}/>
			<Switch>
				<Route exact path="/" />
			{/*<AdminRoute token={token} isAdmin={isAdmin} component={UserPage} exact path="/users"/>*/}
				
				<Route component={Login} path="/login"/>
				<Route component={Logout} path="/logout"/>
				<Route component={RegisterPage} exact path="/register"/>
				<AuthRoute token={token} component={Dashboard} path="/dashboard"/>
				<Route component={EmployeePage} exact path="/employees"/>
				
			</Switch>
		</BrowserRouter>
	)
}

export default App;