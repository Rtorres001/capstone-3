const jwt = require("jsonwebtoken");
const secret = "merng_capstone";

module.exports.createToken = (user) => {
	let data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
		firstName: user.firstName,
		lastName: user.lastName
	}

	return jwt.sign(data, secret, { expiresIn: '2h'})
}