//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("mongoose-double")(mongoose);

const SchemaTypes = mongoose.Schema.Types;

//create a schema
const servicesSchema = new Schema({
	name: {
		type: String,
		required: true,
		trim: true
	},
	price: {
		type: SchemaTypes.Double,
		required: true
	},
	createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
});

//export schema as model
module.exports = mongoose.model("Service", servicesSchema);

