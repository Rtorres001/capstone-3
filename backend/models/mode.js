const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const modeSchema = new Schema ({
	name:{
		type: String,
		required:true
	}
});

//export schema as model
module.exports = mongoose.model ("Mode", modeSchema);
