const BookingRequest = require("../models/bookingRequest");
const Employee = require("../models/employee");
const Service = require("../models/service");
const Mode = require("../models/mode");
const Team = require("../models/team");
const Schedule = require("../models/schedule");
const User = require("../models/user");


//dependency
const bcrypt = require("bcrypt");
const auth = require("../jwt-auth");
const validToken = require("../jwt-verify");

const resolvers= {
	Service: {
		bookingRequests:({ _id }, args) => {
			return BookingRequest.find({ serviceId: _id });
		}
	},

	Team: {
		employees:({ _id }, args) => {
			return Employee.find({teamId: _id});
		},
		bookingRequests: ({ _id}, args)=>{
			return BookingRequest.find({teamId: _id});
		}
	},

	// User
	User: {
		bookingRequests:({ _id }, args) => {
			return BookingRequest.find({userId: _id});
		}
	},
	//Status
	Mode:{
		bookingRequests:({ _id }, args)=>{
			return BookingRequest.find({modeId: _id});
		}
	},


	Employee: {
		team:({ teamId }, args) => {
			return Team.findById(teamId);
		}
	},

	//schedule
	Schedule: {
		bookingRequests:({ _id}, args)=> {
			return BookingRequest.find({ scheduleId: _id});
		}
	},

	BookingRequest:{
		user:({userId}, args)=>{
			return User.findById(userId);
		},
		service: ({serviceId}, args)=>{
			return Service.findById(serviceId);
		},
		mode:({modeId}, args)=>{
			return Mode.findById(modeId);
		},
		team:({ teamId }, args) => {
			return Team.findById(teamId);
		},
		schedule:({scheduleId}, args)=>{
			return Schedule.findById(scheduleId);
		}

	},

	Query:{
		teams: () =>{
			return Team.find({})
		},
		team:(parent, {id})=> {
			return Team.findById(id)
		},
		employees: () =>{
			return Employee.find({})
		},
		employee:(parent, {id})=> {
			return Employee.findById(id)
		},
		users: () =>{
			return User.find({})
		},
		user:(parent, {id})=> {
			return User.findById(id)
		},
		modes: () =>{
			return Mode.find({})
		},
		mode:(parent, {id})=> {
			return mode.findById(id)
		},
		services: () =>{
			return Service.find({})
		},
		service:(parent, {id})=> {
			return Service.findById(id)
		},
		bookingRequests: () =>{
			console.log('yeah boi');
			// console.log(BookingRequest.find({}));
			return BookingRequest.find({})
		},
		bookingRequest:(parent, {id})=> {
			return BookingRequest.findById(id)
		},
		schedules: () =>{
			return Schedule.find({})
		},
		schedule:(parent, {id})=> {
			return Schedule.findById(id)
		}
	},

	Mutation: {
		storeService: (parent,{name, price,})=>{
			let service = new Service ({
				name, price
			})
			return service.save()
		},
		storeTeam: (parent, {name, isActive}) =>{
			let team = new Team ({
				 name, isActive
			})
			return team.save()
		},
		storeEmployee: (parent, {firstName, lastName, position, teamId}) => {
			let employee = new Employee ({
				firstName, lastName, position, teamId
			})
			return employee.save()
		}, 
		storeBookingRequest: (parent, {serviceId, userId, statusId, teamId, bookingDate, scheduleId, contactNumber, address, isPaid}) => {

			let bookingRequest = new BookingRequest ({
				serviceId, userId, statusId, teamId, bookingDate, scheduleId, contactNumber, address, isPaid
			})
			return bookingRequest.save()
		},
		storeSchedule: (parent, {date, timeStart, timeEnd})=>{
			let schedule = new Schedule ({
				date, timeStart, timeEnd
			})
			return schedule.save()
		},
		//hashing of password
		registerUser: (parent, {firstName, lastName, email, password}) => {

			//hash
			let user = new User({
				firstName,
				lastName, 
				email, 
				password : bcrypt.hashSync(password, 8)
			})

			return user.save().then((user, err) => {
				return err ? false : true;
			})
		},
		loginUser: (parent, { email, password}) => {

			//validation
			let query = User.findOne({ email });
			return query.then(user => {
				if(user === null) {
					return null
				} 

				//unhash password
				let isPasswordMatched = bcrypt.compareSync(password, user.password);

				//create login token
				if(isPasswordMatched){
					user.token = auth.createToken(user.toObject());
					return user;
				} else {
					return null;
				}
			})
		}


	}

}

module.exports = resolvers;