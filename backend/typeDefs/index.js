const { gql } = require ("apollo-server-express");

const typeDefs =gql `
	type Service {
		id: ID
		name: String
		price: String
		createdAt: String
		updatedAt: String
		bookingRequests: [BookingRequest]
	}

	type Team {
		id: ID
		name: String
		isActive: Boolean
		createdAt: String
		updatedAt: String
		bookingRequests: [BookingRequest]
		employees: [Employee]
	}

	type User {
		id: ID
		firstName: String
		lastName: String
		email: String
		password: String
		isAdmin: Boolean
		createdAt: String
		updatedAt: String
		token: String
		bookingRequests: [BookingRequest]	
	} 

	type Mode{
		id: ID
		name: String
		bookingRequests: [BookingRequest]
	}

	type Employee{
		id: ID
		firstName: String
		lastName: String
		position: String
		teamId: String
		createdAt: String
		updatedAt: String
		team: Team
	}

	type Schedule {
		id:ID
		date: String
		timeStart: String
		timeEnd: String
		bookingRequests: [BookingRequest]
	}

	type BookingRequest {
		id: ID
		serviceId: String	
		userId: String	
		modeId: String
		teamId: String
		bookingDate: String
		scheduleId: String
		contactNumber: String
		address: String
		isPaid: Boolean
		createdAt: String
		updatedAt: String
		token: String
		user: User
		service: Service
		mode: Mode
		team: Team
		schedule: Schedule
	}

	type Query{
		teams: [Team]
		team(id: ID!): Team
		employees: [Employee]
		employee(id: ID!): Employee
		users: [User]
		user(id: ID!): User
		modes: [Mode]
		mode(id: ID!): Mode
		services:[Service]
		service(id: ID!): Service
		bookingRequests: [BookingRequest!]
		bookingRequest(id: ID!): BookingRequest
		schedules: [Schedule]
		schedule(id: ID!): Schedule
	}

	type Mutation{
		storeService(
			name: String!
			price: String!
		): Service

		storeTeam(
			name: String!
			isActive: Boolean
		): Team

		storeEmployee(
			firstName: String
			lastName: String
			position: String
			teamId: String
		): Employee

		storeBookingRequest(
			serviceId: String
			userId: String
			modeId: String
			teamId: String
			bookingDate: String
			scheduleId: String
			contactNumber: String
			address: String
			isPaid: Boolean
		): BookingRequest

		storeSchedule(
			date: String
			timeStart: String
			timeEnd: String
		): Schedule

		registerUser(
			firstName: String
			lastName: String
			email: String
			password: String
		): Boolean

		loginUser(
			email: String
			password: String
		): User
	}

`;
module.exports = typeDefs;