import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import './style.css';
//imported pages
import App from './app';


// apollo client
const client = new ApolloClient({ uri: 'http://localhost:4333/graphql'});

const root = document.querySelector("#root");
const pageComponent = (
	<ApolloProvider  client={client}>
		<App/>
	</ApolloProvider>
);

ReactDOM.render(pageComponent, root);