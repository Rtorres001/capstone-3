//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const scheduleSchema = new Schema({
	date: {
		type: String,
		required: true
	},
	timeStart: {
		type: String,
		required: true
	},
	timeEnd: {
		type: String,
		required: true
	}
})

//export schema as model
module.exports = mongoose.model("Schedule", scheduleSchema);
