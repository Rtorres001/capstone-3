//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const bookingRequestSchema = new Schema({
	userId: {
		type: String,
		required: true
	},
	serviceId: {
		type: String,
		required: true
	},
	modeId: {
		type: String,
		required: true
	},
	teamId: {
		type: String,
		required: true
	},
	bookingDate: {
		type: String,
		default: Date.now
	},
	scheduleId: {
		type: String,
		required: true
	},
	contactNumber: {
		type: String,
		required: true
	},
	address: {
		type: String,
		required: true
	},
	isPaid: {
		type: Boolean,
		required: true
	},
	createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
});

//export schema as model
module.exports = mongoose.model("BookingRequest", bookingRequestSchema);
