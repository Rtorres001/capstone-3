//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const teamSchema = new Schema ({
	name:{
		type: String,
		required:true
	},
	isActive: {
		type: Boolean,
		required: true
	},
	createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
});

//export schema as model
module.exports = mongoose.model ("Team", teamSchema);
