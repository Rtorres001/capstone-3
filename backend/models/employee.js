//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const employeeSchema = new Schema ({
	firstName: {
		type: String,
		required: true,
		trim: true
	},
	lastName: {
		type: String,
		required: true,
		trim: true
	},
	position:{
		type: String,
		required: true
	},
	teamId: {
		type: String,
		required: true
	},
	createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
});

//export schema as model
module.exports = mongoose.model ("Employee", employeeSchema);
