import React, {Fragment} from 'react';
import { Link } from 'react-router-dom';

import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Navbar } from 'react-bulma-components';

const AppNavbar = props => {
	// console.log(props);

	let endNav = "";
	let startNav = "";

	if(props.token) {
		endNav = (
			<Fragment>
				<Link className="navbar-item has-text-white" to="/dashboard">
					Welcome, { props.firstName} !
				</Link>
				<Link className="navbar-item has-text-white" to="/logout">Logout</Link>
			</Fragment>
			)
			
		startNav = (
			<Fragment>
				<Link className="navbar-item" to="/assets">Assets</Link>
				<Link className="navbar-item" to="/requests">Requests</Link>
			</Fragment>
		)
	} else {
		endNav = (
			<Fragment>
				<Link className="navbar-item" to="/login">Login</Link>
		        <Link className="navbar-item" to="/register">Register</Link>
		    </Fragment>
		)
	}

	if(props.token && props.isAdmin) {
		startNav = (
			<Fragment>
				<Link className="navbar-item" to="/employees">Manage Employee</Link>
				<Navbar.Item>Manage Requests</Navbar.Item>
			</Fragment>
		)
	} 

	return (

	
		<Fragment>

			
			<Navbar className='is-black'>
		      <Navbar.Brand>
		        <Link className="navbar-item" to="/">
		        	<strong>Kirei</strong>
		        </Link>
		        <Navbar.Burger />
		      </Navbar.Brand>
		      <Navbar.Menu>
		       <Navbar.Container>{ startNav }</Navbar.Container>
		        <Navbar.Container position='end'>{ endNav }</Navbar.Container>
		      </Navbar.Menu>
		    </Navbar>
		</Fragment>

	)
}

export default AppNavbar;